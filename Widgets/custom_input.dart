import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inaya/constants.dart';

class CustomInput extends StatelessWidget {

  final String hintText;

  CustomInput( {required this.hintText});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 24,
      ),
      decoration: BoxDecoration(
        color: const Color(0xFFF2F2F2),
        borderRadius: BorderRadius.circular(12),
      ),
      child: TextField(
        decoration: InputDecoration(
          border: InputBorder.none,
          hintText: hintText,
          contentPadding: const EdgeInsets.symmetric(
            horizontal: 24,
            vertical: 22,
          )
        ),
        style: Constants.regularDarkText,
      ),
    );
  }
}
