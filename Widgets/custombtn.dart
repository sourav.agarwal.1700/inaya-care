import 'package:flutter/material.dart';

class CustomBtn extends StatelessWidget {

  final String text;
  final VoidCallback onPressed;
  final bool outlineBtn;

  CustomBtn({required this.text, required this.onPressed, required this.outlineBtn});


  @override
  Widget build(BuildContext context) {

    //bool _outlineBtn = outlineBtn ? false;

    return GestureDetector(
      onTap: onPressed,
      child: Container(
        height: 65,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: outlineBtn? Colors.transparent:Colors.black ,
          border: Border.all(
            color: Colors.black,
            width: 2.0,
          ),
          borderRadius: BorderRadius.circular(12),
        ),
        margin: const EdgeInsets.symmetric(
          horizontal: 24,
          vertical: 8,
        ),
        child: Text(
          text,
          style: TextStyle(
            color: outlineBtn? Colors.black:Colors.white,
            fontWeight: FontWeight.w600,
            fontSize: 16,
          ),
        ),
      ),
    );
  }
}
