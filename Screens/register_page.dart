import 'package:flutter/material.dart';
import 'package:inaya/constants.dart';
import 'package:inaya/screens/login_page.dart';
import 'package:inaya/widgets/custom_input.dart';
import 'package:inaya/widgets/custombtn.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Container(
            color: Color(0xFFFFD4ec),
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  padding: const EdgeInsets.only(
                    top: 25,
                  ),
                  child: const Text(
                    "Create A New Account",
                    textAlign: TextAlign.center,
                    style: Constants.boldHeading,
                  ),
                ),
                Column(
                  children: [
                    CustomInput(
                      hintText: "Email",
                    ),
                    CustomInput(
                      hintText: "Password",
                    ),
                    CustomBtn(
                        text: "Create Account",
                        onPressed: () {
                          print("Pressed Login Button");
                        },
                        outlineBtn: false)
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    bottom: 24,
                  ),
                  child: CustomBtn(
                    text: "Back To Login",
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    outlineBtn: true,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
