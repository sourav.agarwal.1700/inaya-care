import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:inaya/constants.dart';
import 'package:inaya/screens/login_page.dart';

import 'package:inaya/screens/home_page.dart';

class LandingPage extends StatelessWidget {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _initialization,
        builder: (context, snapshot) {
          if (snapshot.hasError) {
            return Scaffold(
              body: Center(
                child: Text("Error: ${snapshot.error}"),
              ),
            );
          }

          //connection initialised - firebase app is running
          if (snapshot.connectionState == ConnectionState.done) {
            return StreamBuilder(
              stream: FirebaseAuth.instance.authStateChanges(),
              builder: (context, streamSnapshot) {
                // if stream snapshot has error
                if (streamSnapshot.hasError) {
                  return Scaffold(
                    body: Center(
                      child: Text("Error: ${streamSnapshot.error}"),
                    ),
                  );
                }

                // connection state active - user login check to be done
                if(streamSnapshot.connectionState == ConnectionState.active) {
                  //get the user

                  Object? _user = streamSnapshot.data ;

                  if(_user == null) {
                    //user not logged in, so head to login page
                    return LoginPage();
                  } else {
                    // return to homepage, user logged in
                    return HomePage();
                  }
                }

                //checking the auth state - loading
                return const Scaffold(
                  body: Center(
                    child: Text(
                      "Checking Authentication...",
                      style: Constants.regularHeading,
                    ),
                  ),
                );
              },
            );
          }

          // connecting to firebase - loading
          return const Scaffold(
            body: Center(
              child: Text(
                "Initialising App...",
                style: Constants.regularHeading,
              ),
            ),
          );
        });
  }
}
