import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:inaya/constants.dart';
import 'package:inaya/screens/register_page.dart';
import 'package:inaya/widgets/custom_input.dart';
import 'package:inaya/widgets/custombtn.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
      child: Container(
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              padding: const EdgeInsets.only(
                top: 25,
              ),
              child: const Text(
                "Welcome User,\nLogin to your account",
                textAlign: TextAlign.center,
                style: Constants.boldHeading,
              ),
            ),
            Column(
              children: [
                CustomInput(
                  hintText: "Email",
                ),
                CustomInput(
                  hintText: "Password",
                ),
                CustomBtn(
                    text: "Login",
                    onPressed: () {
                      print("Pressed Login Button");
                    },
                    outlineBtn: false)
              ],
            ),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 24,
              ),
              child: CustomBtn(
                text: "Create New Account",
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const RegisterPage()
                    ),
                  );
                },
                outlineBtn: true,
              ),
            ),
          ],
        ),
      ),
    ));
  }
}
